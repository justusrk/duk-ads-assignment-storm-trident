
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.spout.IBatchSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

// Fake DukNetwork Events

public class DukNetworkSpout implements IBatchSpout {


    private static final long serialVersionUID = 10L;
    private int batchSize;
    private HashMap<Long, List<List<Object>>> batchesMap = new HashMap<Long, List<List<Object>>>();

    public DukNetworkSpout(int batchSize) {
        this.batchSize = batchSize;
    }

    private static final Map<Integer, String> STUDENT_ID_MAP = new HashMap<Integer, String>();

    static {
        STUDENT_ID_MAP.put(0, "justus.cs21");
        STUDENT_ID_MAP.put(1, "jithin.cs21");
        STUDENT_ID_MAP.put(1, "rafi.cs21");
        STUDENT_ID_MAP.put(3, "renij.cs21");
        STUDENT_ID_MAP.put(4, "farah.cs21");
        STUDENT_ID_MAP.put(5, "amy.cs21");
        STUDENT_ID_MAP.put(2, "rohit.cs21");
        STUDENT_ID_MAP.put(7, "jafeel.cs21");
        STUDENT_ID_MAP.put(8, "noel.cs21");
        STUDENT_ID_MAP.put(9, "bharathwajan.cs21");
        STUDENT_ID_MAP.put(10, "sanoufar.cs21");
        STUDENT_ID_MAP.put(11, "alfas.cs21");
        STUDENT_ID_MAP.put(12, "steve.cs21");
        STUDENT_ID_MAP.put(13, "adithya.cs21");
    }

    private static final Map<Integer, String> EVENT_MAP = new HashMap<Integer, String>();

    static {
        EVENT_MAP.put(0, "HTTP Request - http://example.com/");
        EVENT_MAP.put(1, "HTTPS Request - https://example.com/");
    }

    private List<Object> recordGenerator() {
        final Random rand = new Random();
        int randomNumber = rand.nextInt(14);
        int randomNumber2 = rand.nextInt(2);
        return new Values(STUDENT_ID_MAP.get(randomNumber), EVENT_MAP.get(randomNumber2));
    }

    public void ack(long batchId) {
        this.batchesMap.remove(batchId);

    }

    public void close() {
        // TODO Auto-generated method stub

    }

    public void emitBatch(long batchId, TridentCollector collector) {
        List<List<Object>> batches = this.batchesMap.get(batchId);
        if (batches == null) {
            batches = new ArrayList<List<Object>>();
            ;
            for (int i = 0; i < this.batchSize; i++) {
                batches.add(this.recordGenerator());
            }
            this.batchesMap.put(batchId, batches);
        }
        for (List<Object> list : batches) {
            collector.emit(list);
        }

    }

    public Map getComponentConfiguration() {
        // TODO Auto-generated method stub
        return null;
    }

    public Fields getOutputFields() {
        return new Fields("student-id", "event");
    }

    public void open(Map arg0, TopologyContext arg1) {
        // TODO Auto-generated method stub

    }

}