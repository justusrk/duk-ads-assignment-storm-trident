import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.trident.operation.BaseFilter;
import org.apache.storm.trident.operation.builtin.Count;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Fields;

public class DukNetworkMonitorTopology {



    public static void main(String[] args) {

        Config conf = new Config();
        conf.setMaxSpoutPending(20);

        LocalCluster cluster = null;

        try {
            if (args.length == 0) {
                cluster = new LocalCluster();
                cluster.submitTopology("Count", conf, buildTopology());
            } else {
                conf.setNumWorkers(3);
                StormSubmitter.submitTopology(args[0], conf, buildTopology());
            }
            Thread.sleep(10000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cluster != null) cluster.shutdown();
        }
    }

    public static StormTopology buildTopology() {

        DukNetworkSpout spout = new DukNetworkSpout(10);
        TridentTopology topology = new TridentTopology();


        topology.newStream("spout1", spout)
                .shuffle()
                .groupBy(new Fields("student-id"))
                .aggregate(new Fields("event"), new Count(),
                        new Fields("count"))
                .each(new Fields("count"), new DukNetworkMonitorTopology.PrintFilter())
                .parallelismHint(2);

        return topology.build();
    }


    public static class PrintFilter extends BaseFilter {

        private static final long serialVersionUID = 1L;

        public boolean isKeep(TridentTuple tuple) {
            System.out.println("Printing -------------------------- *********$********** -----------------------");
            System.out.println(tuple);
            return true;
        }

    }

    public static class ArtificialIntelligenceKeywordFilter extends BaseFilter {

        private static final long serialVersionUID = 1L;

        public boolean isKeep(TridentTuple tuple) {
            if (tuple.getString(0).contains("artificial-intelligence")) {
                return true;
            } else {
                return false;
            }
        }

    }

    public static class CyberSecurityKeywordFilter extends BaseFilter {

        private static final long serialVersionUID = 1L;

        public boolean isKeep(TridentTuple tuple) {
            if (tuple.getString(0).contains("cyber-security")) {
                return true;
            } else {
                return false;
            }
        }

    }

}
